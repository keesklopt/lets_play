class Cookie {
    var numChips;
    Cookie({this.numChips});
}

class PerfectCookie {
    final numChips = 42;
    const PerfectCookie();
}

void doStuff(
        [ List<int> list = const [1, 2, 3],
        Map<String, String> gifts = const {
                  'first': 'paper',
                        'second': 'cotton',
                              'third': 'leather'
                                  }]) {
    print('list:  $list');
    print('gifts: $gifts');

}

main() {
    var cookie = new Cookie(numChips: 12);
    var perfectCookie = new PerfectCookie();

    print("An ordinary cookie has ${cookie.numChips} chips");
    print("The perfect cookie has ${perfectCookie.numChips} chips");

    doStuff();

    var a = [1,2,3,4,5];
    var b = a.map((int x) => x * 2);

    print (a);
    print (b);
}
