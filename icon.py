#!/usr/bin/env python3

import math;

wid = 500;
hei = 500;
w3 = math.sqrt(3.0);

def svg(w,h):
    s = '<svg height="%s" width="%d">' % (w,h)
    return s

def circle(w,h,r,t):
    s = '<circle cx="%d" cy="%d" r="%d" stroke="black" fill="red" fill-opacity="0.0" stroke-width="%d" />' % (w/2.0,h/2.0,r,t)
    s += '\>';
    return s;

def triangle(x,y,edge,t,c1,c2):
    edge -= 2.0*t

    part = edge/(2.0*w3)

    y1 = y - edge/2.0;
    x1 = x - part
    y2 = y;
    x2 = x + 2.0*part
    y3 = y + edge/2.0;
    x3 = x1;

    #s = '<polygon  points="%d,%d,%d,%d,%d,%d" style="fill:none;stroke:black;stroke-width:20;stroke-linejoin:"round"/>' % (x1,y1,x2,y2,x3,y3)
    s = '<path d="M%d %d L %d %d L %d,%d Z %d %d" stroke="%s" fill="%s" stroke-width= %d stroke-linejoin= "round"/>' % (x1,y1,x2,y2,x3,y3,x1,y1,c1,c2,t)
    return s;

def revtriangle(x,y,edge,t,c1,c2):
    edge -= 2.2*t

    part = edge/(2.0*w3)

    y1 = y - edge/2.0;
    x1 = x + part
    y2 = y;
    x2 = x - 2.0*part
    y3 = y + edge/2.0;
    x3 = x1;

    #s = '<polygon  points="%d,%d,%d,%d,%d,%d" style="fill:none;stroke:black;stroke-width:20;stroke-linejoin:"round"/>' % (x1,y1,x2,y2,x3,y3)
    s = '<path d="M%d %d L %d %d L %d,%d Z %d %d" stroke="%s" fill="%s" stroke-width= %d stroke-linejoin= "round"/>' % (x1,y1,x2,y2,x3,y3,x1,y1,c1,c2,t)
    return s;

th = 250.0
tw = 250.0*w3 / 2.0;

xc = wid/2.0
yc = hei/2.0

xc1 = xc - (tw/6.0) * 1.0
yc1 = yc - th/4.0

xc2 = xc - (tw/6.0) * 1.0
yc2 = yc + th/4.0

xc3 = xc + (tw/6.0) * 2.0
yc3 = yc

print (svg(wid,hei));
print (circle(wid,hei,160,20));
#print (triangle(wid/2.0,hei/2.0,th,20,'black'));
print (triangle(xc1,yc1,th/2.0,15,'red','red'));
print (triangle(xc2,yc2,th/2.0,15,'blue','blue'));
print (triangle(xc3,yc3,th/2.0,15,'green','green'));
#print (revtriangle(xc,yc,th/2.0,15,'grey','grey'));

