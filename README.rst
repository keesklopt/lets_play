lets\_play
==========

Social tournament manager 

Features
--------

-  Manage tournament generation.
-  Keep scores for the games.
-  Present the scores to app viewers.
-  Accept scores from trusted app users.
-  Send invitations to possible participants.
-  Send alerts to players for matches.
-  Support for poule and final format.


Installation
------------

To get started you need at least flutter and dart. Guides will appear here soon.

flutter
.......

android studio
..............

ios
...

Design
------

The base of the app is a page with an AppBar and a working area. This
working area gets switched with different content based on choices in the
interface.

-  A login page, when logged out.
-  The main page will be big buttons for guiding user to the work pages.
-  A tournament list as a base for logged in or chosen anonymous
   viewers.
-  A settings page shown when selected in the appbar
-  A tournament view if any tournament is chosen on the tournament page
-  A poule/standings page when chosen in the tournament view.
-  A finals page if chosen.

The main page is implemented as a deck of Cards. These card can be
triggered by actions within a page, but also via a menu that possibly
can change per card.

However we start with one menu until that is necessary (3)

Implementation
--------------

This is a list of cleanly rounded points to implement. They should be so consice that they can be done in one day. 
Preferably in order: ) means they are done. Keep the numbers the same even if you switch order, so they remian the same when references elsewhere.
Also make a short chapter of each step under implementation mentioning the number.

By the way: The checkmark in VI : ctrl-v u2173 (or CTRL-k OK)

1)  base setup of flutter app                          ✓
2)  card deck                                          ✓
3)  app bar menu single                                ✓
4)  about screen as the simplest card                  ✓
5)  simple versions of most screens switchable         ✓
5a) get laptop working                                 ✓
6)  login screen for google                            ✓
8)  try facebook login (try is enough)                 ✓ 
7)  add login by email option screen                   ✓ 
11) Anymous trial.. (what data does it provide ?)      ✓
13) Complete object handling at least two logins 
9)  tournament list screen
10) tournament create screen
12) continue design

(1)base setup
.............

This was just copied from an example app, so the base is now a lib/main.dart with an app bar and a start page. 

(2) card deck
.............

This is an array of structure that can be chosen. When chosen the title in the appbar becomes the one in the chosen row, and the content of the main card is fille bij the sw() function of the same row. There is also an icondata member that could be used as menu item. 
Also when we go for switchable menus, that would also become a member of the row struct.

(3) app bar single menu
.......................

Until we need a switchable menu, we manage with one single one:

- logout
- settings

I think with these two we can do with icons in the topbar, also add a 3 dot menu with only 'about' below it.

(4) about screen and first card
...............................

This is about as simple as they get, an about message, maybe a link to klopt.org. This should be a structure that can be fed to the screen switcher. 

(5) simple version of most screens switchable
.............................................

The main point of this is to be able to add new screens by simply filling in an array with : title, icon and a function than simply returns a widget. That last function is of course a big one but the mechanism should be clear.
Also this structure will be extended with a menu as well.

This one is done when you can run a multiscreen app and switch between cards.

(5b) get the laptop working
...........................

b) because it was added in between, but i think it's a biggy. I need to use the laptop on vacation, when it's too warm upstairs, perhaps in the train and bus ?
It just needs to work. 

Main issues : flutter cli works !, but i (1) cannot log in to google because a sha1 hash for the laptop needs to be installed on firebase. 
But to do that i need to run android studio. And (2) that just does not show any devices ?!

I have waisted a complete day on this. Flutter still work CLI. Here is an alternative way to get the SHA1 key : 

keytool -list -v -keystore ~/.android/debug.keystore -alias androiddebugkey -storepass android -keypass android 

Pick the SHA 1 line and fill that in this page :

https://console.firebase.google.com/project/lets-play-it/settings/general/android:org.klopt.letsplay

Or in the lets-play-it project click on the gear icon next to project overview and then "Project settings" You can add fingerprints there. YOu need to do this for every development (debug) machine you want to use !! (Document this)


(6) Get through google login + logout
.....................................

The login module should be a module that can be reused again and again. Also later on new logins could be added.

An app initializes itself, and implements the following functions.

* .build_main()
* .build_login()
* .login()
* .logout()

current task
------------

Install webdev and dart2
........................


install build_runner first in pubspec.yaml 

::

  dev_dependencies:
    flutter_test:
      sdk: flutter

    mockito: any
    build_runner: ^0.9.0
    build_test: ^0.10.2
    build_web_compilers: ^0.4.0

Mockito is there for unittesting.
Then run some upgrades :

::

    flutter upgrade
    flutter packages get
    pub global activate webdev

testing
-------

These should be unit-testable and widget testable. Maybe even a complete login bu Intergration test should be built.

build_main is the normal build function for the application and is called by build() when logged in. build_login() is a function thas builds just one widget as child : the LoginWidget. 

The create User and change displayname part should be combined into one register function. 
Also note the ander@klopt.org is NOT a real email adress and so the value isEmailVerified is false. See if this can be fixed automatically. 

::

    FirebaseUser(
    {
        providerId: firebase, 
        uid: ZeM12PdN9FciQt4LO0jB5qKHBtp1, 
        displayName: Anders Andersson, 
        email: ander@klopt.org, 
        isAnonymous: false, 
        isEmailVerified: false, 
        providerData: [
            {
                providerId: firebase, 
                uid: ZeM12PdN9FciQt4LO0jB5qKHBtp1, 
                displayName: Anders Andersson, 
                email: ander@klopt.org
            }, {
                providerId: password, 
                uid: ander@klopt.org, 
                displayName: Anders Andersson, 
                email: ander@klopt.org
            }
        ]
    })

(7) Add login by email option (screen)
......................................

Note that this also needs a registration screen !! And a name+password form.
The other(s) are social media accounts that have already handled the registration. 

It is vital that we arrive at a usable user structure or null for anynymous,  
or try the anonymous option of firebase ! (see 11)

Now the main pattern in logging in is : 

* enter the login screen
* choose a method of login 
* go to a screen presenting a login name and password
* be allowed into the app as a use or as anonymous.

Now step 3 has to be done manually for email while the others have their own mechanism.

(8) Try facebook login 
......................

There are functions for twitter and facebook in the firabase-flutter plugin. But they need a secondary libray as well. : https://github.com/flutter/plugins/blob/master/packages/firebase_auth/lib/firebase_auth.dart

For facebook it is : https://pub.dartlang.org/packages/flutter_facebook_connect
Someone here gave a headstart : https://stackoverflow.com/questions/46343303/how-do-i-use-firebase-auths-signinwithfacebook-method.

but i haven't got it working.
Also when logging in to the facebook page i was told my app was due for a review before 1 august, about the privacy laws. i
For that see : https://developers.facebook.com/docs/apps/review/faqs/#graph-api-3-0

(11) Login screen email
.......................

(13) Complete login bubl
........................


Documentation
-------------

Note that this is rst, but you can
reformat this to .md (markdown) or other formats with pandoc !

::

    apt-get install pandoc
    pandoc -i README.rst -o README.md

InstantRst Rst for vim
......................

To have this document itself rendered in .rst in a local browser :

http://localhost:5676

You need to install a couple of items :

vundle
~~~~~~

Bundle for vim, you can easily install plugins in vim once this plugin is installed.

::

    git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim

This will put everything in the right place, now add this to your .vimrc :

::

    set nocompatible

    filetype off                  " required

    " set the runtime path to include Vundle and initialize
    set rtp+=~/.vim/bundle/Vundle.vim
    call vundle#begin()
    " alternatively, pass a path where Vundle should install plugins
    "call vundle#begin('~/some/path/here')
    "
    "Bundle 'Rykka/riv.vim'
    Bundle 'Rykka/InstantRst'
    " let Vundle manage Vundle, required
    Plugin 'VundleVim/Vundle.vim'

    call vundle#end()            " required

    filetype plugin indent on    " required

It contains a plugin that is very handy for editing restructuredtext. And one commmented out because i don't particularly found that very usefull.

Riv knows RST format inside vim but the folding end general usage is not of much benifit. The second plugin however : instantrst is very usefull and described in the next section.

If you open up vim with this .vimrc and type :

::

    :PluginInstall

 It will install the Bundles you added, to remove one (like riv) you comment it out and run :

::

    :PluginClean


Now you have a new plugin called InstantRst.

Instant rst
~~~~~~~~~~~

Now the plugin itself.  This plugin shows the result from you RST file inside your browser, usually at port :5676.

http://localhost:5676

It opens up automatically if you enter the vim command below and you can see all editing while you type.


::

    " in vim type
    :InstantRst


It should either open a pafe in chrome of come with some error, but note that this page only 'stays alive' while you are in the same vim file. Opening another file will kill it already.


pandoc
......

Note that this is markdown, but you can reformat this to .rst with pandoc !

    apt-get install pandoc
    pandoc -i README.md -o README.rst

This uses flutter and android studio, installation guides follow later.


Support
-------

If you are having issues, please let us know.
We have a mailing list located at: project@google-groups.com

[documentation](https://flutter.io/).

License
-------

The project is licensed under the BSD license.
