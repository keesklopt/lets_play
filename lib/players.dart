import 'package:flutter/material.dart';

import 'playerform.dart';

class PlayersEditPage extends StatefulWidget {
  //final Future<List> players;
  final PlayersEditState mystate = new PlayersEditState();

  PlayersEditPage();

  @override
  PlayersEditState createState() {
    return mystate;
  }
}

class Player {
  String name;
  String email;
  String fullname;
  String phone;

  Player(String name, String email, String fn, String phone) {
    this.name = name;
    this.email = email;
    this.fullname = fn;
    this.phone = phone;
  }

  Player.fromJson(Map<String, dynamic> j)
    : name = j['name'],
      email = j['email'];

  Map<String, dynamic> toJson() =>
  {
      'name': name,
      'email': email,
  };
}

class PlayerListItem extends StatelessWidget {
  final Player player;

  PlayerListItem(this.player);

  @override
  Widget build(BuildContext context) {
    return new Padding(
      padding: new EdgeInsets.all(6.0),
      child: new Row(children: [
        new Expanded(
            child: new Text(
          player.fullname,
          textScaleFactor: 0.8,
          textAlign: TextAlign.left,
        )),
        new Expanded(
            child: new Text(
          player.name,
          textScaleFactor: 1.8,
          textAlign: TextAlign.left,
        )),
        new Flexible(
            child: new Column(children: [
                new Text(
                    player.email.toString(),
                    textScaleFactor: 0.8,
                    textAlign: TextAlign.left,
                ),
                new Text(
                    player.phone.toString(),
                    textScaleFactor: 0.8,
                    textAlign: TextAlign.right,
                    style: new TextStyle(
                    color: Colors.grey,
                    ),
                ),
            ], crossAxisAlignment: CrossAxisAlignment.start)),
            ]),
        );
    }
}

class PlayersEditState extends State<StatefulWidget> {
  bool loading = true;

  List<Player> players = new List();

  void _addPlayer() {
    Navigator.push(context, MaterialPageRoute(builder: (context) => PlayerForm(players: players, currentPlayer: Player("b","c","meel",""))));
     setState(() {
       players.add(new Player("Joop", "Joop Klep", "joop@hier.daar", "Ome"));
     });
  }

  @override
  Widget build(BuildContext context) {
    //final TextStyle textStyle = Theme.of(context).textTheme.display1;

    return Scaffold(
      appBar: AppBar(
        title: Text("Manage players"),
      ),
      body: new ListView(
        children: players.map((Player player) {
          return PlayerListItem(player);
        }).toList(),
      ),
      floatingActionButton: new FloatingActionButton(
        onPressed: _addPlayer,
        tooltip: 'Add new player entry',
        child: new Icon(Icons.add),
      ),
    );
  }
}
