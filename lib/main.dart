// Copyright 2017 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'login.dart';
import 'about.dart';
import 'competition.dart';
import 'players.dart';
import 'dart:io';

import 'package:bubl/bubl.dart';

const MAJOR = 0;
const MEDOR = 0;
const MINOR = 2;

String versionString() {
  return MAJOR.toString() + "." + MEDOR.toString() + MINOR.toString();
}

// This app is a stateful, it tracks the user's current choice.
class LetsPlayApp extends StatefulWidget {
  @override
  _LetsPlayState createState() {
    return new _LetsPlayState();
  }
}

class ScreenCard {
  ScreenCard({this.title, this.icon, this.sw});

  String title;
  IconData icon;
  Function sw;
}

class ScreenWidget extends StatelessWidget {
  final ScreenCard current = cards[0];

  @override
  Widget build(BuildContext context) {
    final TextStyle textStyle = Theme.of(context).textTheme.display1;
    return new Card(
      color: Colors.white,
      child: new Center(
        child: new Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            new Icon(current.icon, size: 128.0, color: textStyle.color),
            new Text(current.title, style: textStyle),
          ],
        ),
      ),
    );
  }
}

class _LetsPlayState extends State<LetsPlayApp> {
  ScreenCard current;
  FirebaseUser user;
  bool init = false;

  _LetsPlayState() {
    if (currentUser != null) {
      current = cards[1];
    } else {
      current = cards[0];
    }
  }

  void _select(ScreenCard choice) {
    setState(() {
      current = choice;
      if (choice == cards[0]) {
        this.init = false;
        this.user = null;
      }
    });
  }

  void loggedIn(fbu) {
    this.user = fbu;
    this.init = true;
    String title = "Welkom onbekende.";
    if (this.user != null) {
      title = this.user.isAnonymous == true
          ? "Open tournaments"
          : "Welkom " + this.user.displayName;
    }
    cards[1].title = title;
    this._select(cards[1]);
  }

  @override
  Widget build(BuildContext context) {
    if (this.init != false) {
      return build1(context);
    } else
      return buildLogin(context);
  }

  Widget buildLogin(BuildContext context) {
    return new MaterialApp(
        home: new Scaffold(
            appBar: new AppBar(title: new Text("Log in")),
            body: new Container(
                child: new LoginPage(title: 'Login page', cb: loggedIn))));
  }

  Widget build1(BuildContext context) {
    Widget sw = current.sw();
    return new MaterialApp(
      home: new Scaffold(
        // app bar at the top
        appBar: new AppBar(title: new Text(current.title), actions: <Widget>[
          new IconButton(
            icon: new Icon(cards[0].icon),
            onPressed: () {
              _select(cards[0]);
            },
          ),
          new PopupMenuButton<ScreenCard>(
              onSelected: _select,
              itemBuilder: (BuildContext context) {
                return [
                  new PopupMenuItem<ScreenCard>(
                      value: cards[2], child: const Text("About")),
                ];
              }),
        ]),
        body: new Container(
            //child: new ChoiceCard(choice: _selectedChoice),
            //child: new LoginPage(title: 'Login page')
            child: sw),
      ),
    );
  }
}

/*
    void findCompetion() { 
        Navigator.push(context, 
            MaterialPageRoute(builder: (context) => CompetitionListPage()));
    }
*/

    void callBack() { 
        print ("Ok");
    }

    void newTournament() { 
        print ("Nieuw");
    }


/*
    void editPlayers() { 
        Navigator.push(context, 
            MaterialPageRoute(builder: (context) => PlayersEditPage()));
    } 
*/

    void exitApp() { 
        print ("Bye");
        exit(0);
    }

Widget makeMain() {

   final colors = [ 0xFF9e6071, 0xFF782b73, 0xFF39399e, 0xFF60929e,0xFF495c78, 0xFF399e8a, 0xFF399e46, 0xFF627849, 0xFF9e8a39, 0xFF916f59, 0xFF913434];

   var c=0;
   List<ButtonData> list = [
        ButtonData(caption: "Zoek Toernooien", icondata: Icons.search, color: Color(colors[c++]), next: CompetitionListPage()),
        ButtonData(caption: "Events", icondata: Icons.date_range, color:  Color(colors[c++]),next: CompetitionListPage()),
        ButtonData(caption: "Spelers", icondata: Icons.people, color: Color(colors[c++]), next: PlayersEditPage()),
        ButtonData(caption: "Instellingen", icondata: Icons.settings, color: Color(colors[c++]),next: AboutPage()),
    ];

  return new DispatchPage(list);
}

Widget makeAbout() {
  return new AboutPage();
}

Widget makeLogin() {
  return new LoginPage();
}

List<ScreenCard> cards = <ScreenCard>[
  new ScreenCard(
      title: 'Log in',
      icon: new IconData(0xf342, fontFamily: "mdi"),
      sw: makeLogin),
  new ScreenCard(
      title: 'Tournaments',
      icon: new IconData(0xf343, fontFamily: "mdi"),
      sw: makeMain),
  //new ScreenCard(title: 'Instellingen', icon: IconData(0xf493, fontFamily: "mdi"), sw: null),
  new ScreenCard(
      title: 'About',
      icon: new IconData(0xf342, fontFamily: "mdi"),
      sw: makeAbout),
];

void main() {
  // runApp inflates the given widget onto the screen
  runApp(new LetsPlayApp());
}
