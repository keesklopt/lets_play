import 'package:flutter/material.dart';
import 'main.dart';

String aboutMessage =
    "Let's Play ! " + versionString() + "\nGoogle and email login";

class AboutPage extends StatelessWidget {
  AboutPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  Widget build(BuildContext context) {
    final TextStyle textStyle = Theme.of(context).textTheme.display1;
    return new Card(
      color: Colors.white,
      child: new Center(
        child: new Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            new Text(aboutMessage, style: textStyle),
          ],
        ),
      ),
    );
  }
}
