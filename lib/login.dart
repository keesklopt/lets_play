// Copyright 2017 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

//import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:google_sign_in/google_sign_in.dart';
//import 'package:flutter_facebook_login/flutter_facebook_login.dart';

final FirebaseAuth _auth = FirebaseAuth.instance;
final GoogleSignIn _googleSignIn = new GoogleSignIn();

FirebaseUser currentUser;

class SocialLogin {
  const SocialLogin({this.title, this.icon, this.color});
  final Color color;
  final String title;
  final IconData icon;
}

const List<SocialLogin> choices = const <SocialLogin>[
  const SocialLogin(
      title: 'facebook',
      icon: const IconData(0xf20c, fontFamily: "mdi"),
      color: const Color(0xFF3b5999)),
  const SocialLogin(
      title: 'google',
      icon: const IconData(0xf2ad, fontFamily: "mdi"),
      color: const Color(0xFFdd4b39)),
  const SocialLogin(
      title: 'email',
      icon: const IconData(0xf1ee, fontFamily: "mdi"),
      color: const Color(0xFF02b875)),
  const SocialLogin(
      title: 'anon',
      icon: const IconData(0xf5f9, fontFamily: "mdi"),
      color: const Color(0xFF4e342e)),
  const SocialLogin(
      title: 'none',
      icon: const IconData(0xF2a0, fontFamily: "mdi"),
      color: const Color(0xFFaaaaaa)),
  const SocialLogin(
      title: 'twitter',
      icon: const IconData(0xf544, fontFamily: "mdi"),
      color: const Color(0xFF55acee)),
];

/*
_doSignInWithFacebook() async {
  // final _facebookConnect = new FacebookConnect(
  // appId: '205583516848135',
  // clientSecret: '8431499b1a167dbb7d067a2d940a02b0');
  final FacebookLogin facebookSignIn = new FacebookLogin();
  facebookSignIn.loginBehavior = FacebookLoginBehavior.webOnly;
  final FacebookLoginResult result =
      await facebookSignIn.logInWithReadPermissions(['email']);

  print(result.status);

  // FacebookOAuthToken token = await _facebookConnect.login();
  // final FirebaseUser user=await
  // _auth.:tgnInWithFacebook(accessToken:token.access);
  // assert(user.email != null);
  // assert(user.displayName != null);
  // assert(!user.isAnonymous);
  // assert(await user.getIdToken() != null);

  // final FirebaseUser currentUser = await _auth.currentUser();
  // assert(user.uid == currentUser.uid);

  // print ('signInWithFacebook succeeded: $user');

  return null;
}
*/

_doSignInWithGoogle() async {
  final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
  final GoogleSignInAuthentication googleAuth = await googleUser.authentication;
  final user = _auth.signInWithGoogle(
    accessToken: googleAuth.accessToken,
    idToken: googleAuth.idToken,
  );
  // assert(user.email != null);
  // assert(user.displayName != null);
  // assert(!user.isAnonymous);
  // assert(await user.getIdToken() != null);

  //final FirebaseUser currentUser = await _auth.currentUser();
  //assert(user.uid == currentUser.uid);

  print('signInWithGoogle skipped: $user');

  return user;
}

_doSignInWithEmail() async {
  final FirebaseUser user = await _auth.signInWithEmailAndPassword(
    email: "ander@klopt.org",
    password: "toedeled0kie",
  );
  // assert(user.email != null);
  // assert(user.displayName != null);
  // assert(!user.isAnonymous);
  // assert(await user.getIdToken() != null);

  //currentUser = await _auth.currentUser();
  // assert(user.uid == currentUser.uid);

  print('signInWithEmail succeeded: $user');

  return user;
}

_doSignInAnonymously() async {
  final FirebaseUser user = await _auth.signInAnonymously();
  assert(user != null);
  assert(user.isAnonymous);
  assert(!user.isEmailVerified);
  assert(await user.getIdToken() != null);
  if (Platform.isIOS) {
    // Anonymous auth doesn't show up as a provider on iOS
    assert(user.providerData.isEmpty);
  } else if (Platform.isAndroid) {
    // Anonymous auth does show up as a provider on Android
    print(user);
    // assert(user.providerData.length == 1);
    assert(user.providerData[0].providerId == 'firebase');
    assert(user.providerData[0].uid != null);
    // assert(user.providerData[0].displayName == null);
    assert(user.providerData[0].photoUrl == null);
    // assert(user.providerData[0].email == null);
  }

  //final FirebaseUser currentUser = await _auth.currentUser();
  //assert(user.uid == currentUser.uid);

  return user;
}

_doNotSignIn() async {
  return null;
}

signInWith(SocialLogin sl, Function cb) {
  var u;
  switch (sl.title) {
    /*
	    * case "facebook": print ("Yep, facebook"); u
	    * =_doSignInWithFacebook(); break; 
	    */
    case "google":
      u = _doSignInWithGoogle();
      break;
    case "email":
      // _doSignInAnonymously();
      u = _doSignInWithEmail();
      break;
    case "anon":
      u = _doSignInAnonymously();
      break;
    case "none":
      u = _doNotSignIn();
      break;
    default:
      print("Implement : $sl.title");
      break;
  }
  u.then((usr) {
    cb(usr);
  });
}

class LoginButton extends StatelessWidget {
  LoginButton({this.sl, this.cb});

  final SocialLogin sl;
  final Function cb;

  @override
  Widget build(BuildContext context) {
    var caption = "Inloggen met ${sl.title}";

    if (this.sl.title == "anon") caption = "Dank je, ik kijk alleen uitslagen.";
    if (this.sl.title == "none") caption = "Niet inloggen.";

    // the recognizable icon
    IconButton icon = new IconButton(
        icon: new Icon(this.sl.icon),
        onPressed: () {
          signInWith(this.sl, cb);
        },
        color: this.sl.color);

    // button stating what to log in to
    RaisedButton button = new RaisedButton(
        child: new Text(caption),
        color: this.sl.color,
        textColor: Colors.white,
        onPressed: () {
          signInWith(this.sl, this.cb);
        });

    ButtonTheme bt = new ButtonTheme(minWidth: 600.0, child: button);

    // now present them in one row
    Row loginrow = new Row(children: <Widget>[icon, new Expanded(child: bt)]);

    return loginrow;
  }
}

class MyTextBox extends StatelessWidget {
  final String text;

  MyTextBox({this.text = "nix"});

  @override
  Widget build(BuildContext ctx) {
    var txt = new RichText(
        text: new TextSpan(
            text: this.text,
            style: new TextStyle(
              color: Colors.black.withOpacity(0.9),
            )));
    return new Container(
      height: 100.0,
      width: 200.0,
      padding: new EdgeInsets.all(10.0),
      decoration: new ShapeDecoration(
        color: Colors.grey,
        shape: new RoundedRectangleBorder(
          borderRadius: new BorderRadius.all(new Radius.circular(20.0)),
        ),
        // side: new BorderSide(color: Colors.white)
      ),
      child: new Center(
        child: txt,
      ),
    );
  }
}

class LoginPage extends StatelessWidget {
  LoginPage({Key key, this.title, this.cb}) : super(key: key);

  final Function cb;
  final String title;

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: new Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          //new MyTextBox(text: "Explanation on why to Log in"),
          new LoginButton(sl: choices[1], cb: cb),
          new LoginButton(sl: choices[2], cb: cb),
          //new MyTextBox(text: "Explanation on when not"),
          new LoginButton(sl: choices[3], cb: cb),
          new LoginButton(sl: choices[4], cb: cb),
        ],
      ),
    );
  }
}
