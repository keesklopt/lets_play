import 'package:flutter/material.dart';
import 'dart:async';

class CompetitionListPage extends StatefulWidget {
  final Future<List> complist;

  CompetitionListPage({this.complist});

  @override
  _CompetitionListState createState() => new _CompetitionListState();
}

class Competition {
    String loc="loc";
    String name;
    String owner;
    num players;
    num poules;

    Competition(String l, String n, String owner, int players, int poules) 
    {
        this.loc=l;
        this.name=n;
        this.owner=owner;
        this.players=players;
        this.poules=poules;
    }
}

class _CompetitionListState extends State<StatefulWidget>
{
  bool loading = true;

  List<Competition> items = [
    new Competition('hoek', 'holland', 'william', 32, 4),
    new Competition('naaldwijk', 'darterites', 'someone', 16, 4),
  ];

  @override
  Widget build(BuildContext context) {
    //final TextStyle textStyle = Theme.of(context).textTheme.display1;

    final cols1 = [
      new DataColumn(
        // expand does not work :
        label: new Expanded(child: Text('Name')),
      ),
      new DataColumn(
        label: const Text('Location'),
      ),
      new DataColumn(
        label: const Text('Organizer'),
      ),
      /* too much for the list 
      new DataColumn(
        label: const Text('Spelers'),
      ),
      new DataColumn(
        label: const Text('Poules'),
      ),
        */
    ];

    List<DataRow> rows1 = [];

    for (Competition x in items) {
        DataRow r = new DataRow(
           cells: [
              new DataCell(new Text(x.name)),
              new DataCell(new Text(x.loc)),
              new DataCell(new Text(x.owner)),
              //new DataCell(new Text(x.players.toString())),
              //new DataCell(new Text(x.poules.toString())),
            ]);
        rows1.add(r);
    } 

    // size of the screen, to divide buttons equally over the screen
    var size = MediaQuery.of(context).size;
    final double itemHeight = (size.height - kToolbarHeight - 24) / 2;
    final double itemWidth = size.width / 2;

    return Scaffold(
        appBar: AppBar(
            title: Text("Search tournaments"),
        ),
        body: GridView.count(
            crossAxisCount: 2,
            controller: new ScrollController(keepScrollOffset: false),
            scrollDirection: Axis.vertical,

            childAspectRatio: (itemWidth / itemHeight),
            children: [
                new DataTable(columns: cols1, rows: rows1),

                    //new MainButton("Tournooien", 'assets/podium.png', Color(0X1F8F0000)),
                    //new MainButton("Nieuw tournooi", 'assets/trophy.png', Color(0X6F1F0000)),
                    //new MainButton("Spelers", 'assets/player.png', Color(0X6F8F7000)),
                    //new MainButton("Instellingen", 'assets/gear.png', Color(0X6F8F0000)),
            ]
         ),
      );
  }
}
