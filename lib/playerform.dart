import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:contacts_service/contacts_service.dart';
import 'package:path_provider/path_provider.dart';


import 'package:intl/intl.dart';

import 'util.dart';
import 'players.dart';

import 'dart:convert';
import 'dart:io';
import 'dart:async';

// Create a Form Widget
class PlayerForm extends StatefulWidget {
  final List<Player> players;
  final Player currentPlayer;

  PlayerForm({Key key, this.players, this.currentPlayer}) : super(key: key);

  @override
  PlayerFormState createState() {
    return PlayerFormState(this.currentPlayer);
  }
}

/*
class ContactItem extends StatelessWidget {
  const ContactItem(this.entry);

  final ContactItem contact;

  Widget _buildTiles(Contact root) {
    if (root.children.isEmpty) return ListTile(title: Text(root.title));
    return Text(root.title),
  }

  @override
  Widget build(BuildContext context) {
    return _buildTiles(entry);
  }
}
*/

class ContactList extends StatefulWidget {
  final List<Contact> contacts;
  final Player currentPlayer;

  ContactList(this.contacts, this.currentPlayer);

  @override
  ContactListState createState() {
    return ContactListState(contacts, currentPlayer);
  }
}

class ContactListState extends State<ContactList> {
  List<Contact> contacts;
  List<Contact> filtered;
  Player currentPlayer;
  ContactListState(this.contacts, this.currentPlayer) {
    this.filtered = contacts;
  }

  // Build a Form widget using the _formKey we created above
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          children: [
            Text("Start met : "),
            Expanded(
              child: TextField(
                onChanged: (text) {
                  setState(() {
                    this.filtered = contacts
                        .where((l) => l.displayName
                            .toLowerCase()
                            .startsWith(text.toLowerCase()))
                        .toList();
                    printb(this.filtered.length.toString());
                  });
                },
              ),
            ),
          ],
        ),
        Expanded(
          child: ListView.builder(
              padding: const EdgeInsets.symmetric(horizontal: 16.0),
              scrollDirection: Axis.vertical,
              itemCount: this.filtered.length,
              itemBuilder: (context, index) {
                return ListTile(
                    leading: const Icon(Icons.person),
                    title: Text(this.filtered[index].displayName),
                    enabled: true,
                    onTap: () {
                      printc("Tapped " + index.toString());
                      //currentPlayer.name = this.filtered[index].givenName;
                      Navigator.pop(context, this.filtered[index]);
                    });
              }),
        ),
      ],
    );
  }
}

// Create a corresponding State class. This class will hold the data related to
// the form.
class PlayerFormState extends State<PlayerForm> {
  // Create a global key that will uniquely identify the Form widget and allow
  // us to validate the form
  //
  // Note: This is a GlobalKey<FormState>, not a GlobalKey<PlayerFormState>!
  final _formKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  Player currentPlayer;

  PlayerFormState(this.currentPlayer);

  Contact contact;

  bool isValidPhoneNumber(String input) {
    final RegExp regex = new RegExp(r'^\(\d\d\d\)\d\d\d\-\d\d\d\d$');
    return regex.hasMatch(input);
  }

  bool isValidDob(String dob) {
    if (dob.isEmpty) return true;
    var d = convertToDate(dob);
    return d != null && d.isBefore(new DateTime.now());
  }

  bool isValidEmail(String input) {
    final RegExp regex = new RegExp(
        r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$");
    return regex.hasMatch(input);
  }

  DateTime convertToDate(String input) {
    try {
      var d = new DateFormat.yMd().parseStrict(input);
      return d;
    } catch (e) {
      return null;
    }
  }

  void showMessage(String message, [MaterialColor color = Colors.red]) {
    printc(_scaffoldKey.currentState.toString());
    _scaffoldKey.currentState.showSnackBar(
        new SnackBar(backgroundColor: color, content: new Text(message)));
  }

  void writePlayers(List<Player>players) async {
        Directory appDocDir = await getApplicationDocumentsDirectory();
        String appDocPath = appDocDir.path;
        print (appDocPath);
        File f = File('$appDocPath/players.txt');
        String j = json.encode(players);
        print (j);
        f.writeAsString(j);
  }

  void _submitForm() {
    final FormState form = _formKey.currentState;

    if (!form.validate()) {
      showMessage('Form is not valid!  Please review and correct.');
    } else {
      form.save(); //This invokes each onSaved event

      printc('De nieuwe speler is opgeslagen ...');
      printc('Speler Naam: ${currentPlayer.name}');
      printc('Volledige Naam: ${currentPlayer.fullname}');
      //print('Dob: ${currentPlayer.dob}');
      printc('Phone: ${currentPlayer.phone}');
      printc('Email: ${currentPlayer.email}');
      //print('Favorite Color: ${currentPlayer.favoriteColor}');
      printc('========================================');
      printc('Submitting to back end...');
      printc('TODO - we will write the submission part next...');

      widget.players.add(currentPlayer);
      writePlayers(widget.players);
      Navigator.pop(context);
    }
  }

  void _openPicker() async {
    Iterable<Contact> contacts = await ContactsService.getContacts();

    contact = await showDialog(
        context: context,
        builder: (_) => new Dialog(
              child: new ContactList(contacts.toList(), currentPlayer),
            ));
    setState(() {
      print(contact.displayName);
      currentPlayer.name = contact.displayName;
      //currentPlayer.phone = contact.phones as String;
    });
  }

  @override
  Widget build(BuildContext context) {
    printb ("Rebuilding ");
    printred (currentPlayer.name);
    return new Scaffold(
      key: _scaffoldKey,
      appBar: new AppBar(title: Text("Vul de spelers gegevens in")),
      body: new Form(
          key: _formKey,
          autovalidate: true,
          // list o fields :
          child: new ListView(
            // name row
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            children: <Widget>[
              new TextFormField(
                  decoration: const InputDecoration(
                    icon: const Icon(Icons.person_outline),
                    hintText: 'Naam in de wedstrijden',
                    labelText: 'Spelersnaam',
                  ),
                  controller: TextEditingController(text: currentPlayer.name.toString()),
                  inputFormatters: [new LengthLimitingTextInputFormatter(30)],
                  validator: (val) =>
                      val.isEmpty ? 'Een unieke naam is nodig' : null,
                  //onSaved: (val) => currentPlayer.name = val,
                  onSaved: (val) {
                    printc(val);
                    currentPlayer.name = val;
                  }),
              // full name row
              new TextFormField(
                decoration: const InputDecoration(
                  icon: const Icon(Icons.person),
                  hintText: 'Voor en achternaam',
                  labelText: 'Volledige naam',
                ),
                initialValue: currentPlayer.fullname.toString(),
                inputFormatters: [new LengthLimitingTextInputFormatter(30)],
                //validator: (val) => val.isEmpty ? 'Volledige naam is nodig' : null,
                onSaved: (val) => currentPlayer.fullname = val,
              ),
              new TextFormField(
                decoration: const InputDecoration(
                  icon: const Icon(Icons.phone),
                  hintText: 'Voor uitnodigen is een telefoon nummer nodig',
                  labelText: 'Telefoon',
                ),
                initialValue: currentPlayer.phone.toString(),
                keyboardType: TextInputType.phone,
                inputFormatters: [
                  new WhitelistingTextInputFormatter(
                      new RegExp(r'^[()\d -]{1,15}$')),
                ],
                //validator: (value) => isValidPhoneNumber(value) ? null : 'Vul een geldig telefoonnummer in',
                onSaved: (val) => currentPlayer.phone = val,
              ),
              new TextFormField(
                decoration: const InputDecoration(
                  icon: const Icon(Icons.email),
                  hintText: 'Enter a email address',
                  labelText: 'Email',
                ),
                initialValue: currentPlayer.email.toString(),
                keyboardType: TextInputType.emailAddress,
                //validator: (value) => isValidEmail(value) ? null : 'Please enter a valid email address',
                onSaved: (val) => currentPlayer.email = val,
              ),
              /* picklist example, leave it as example
        new InputDecorator(
          decoration: const InputDecoration(
            icon: const Icon(Icons.color_lens),
            labelText: 'Color',
          ),
          isEmpty: _color == '',
          child: new DropdownButtonHideUnderline(
            child: new DropdownButton<String>(
              value: _color,
              isDense: true,
              onChanged: (String newValue) {
                setState(() {
                  currentPlayer.favoriteColor = newValue;
                  _color = newValue;
                });
              },
              items: _colors.map((String value) {
                return new DropdownMenuItem<String>(
                  value: value,
                  child: new Text(value),
                );
              }).toList(),
            ),
          ),
        ),
        */
              new Container(
                  padding: const EdgeInsets.only(left: 40.0, top: 20.0),
                  child: new RaisedButton(
                    child: const Text('Opslaan'),
                    onPressed: _submitForm,
                  )),
            ],
          )),
      floatingActionButton: new FloatingActionButton(
        onPressed: _openPicker,
        tooltip: 'Open contact picker',
        child: new Icon(Icons.contacts),
      ),
    );
  }
}
