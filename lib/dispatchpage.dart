import 'package:flutter/material.dart';
import 'dart:io';

import 'competition.dart';
import 'players.dart';

class DispatchPage extends StatefulWidget {

    @override
    _DispatchState createState() => new _DispatchState();
}

class MainButton extends StatelessWidget
{
    final String caption;
    final String image;
    final Color color;
    final Function cb;

    const MainButton({Key key,this.caption, this.image, this.color, this.cb}): super (key: key);

    Widget build(BuildContext context) {

        var mb = new MaterialButton( 

            color: this.color,
            elevation: 3.0,
            child: Stack(

            children: [
                new Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [

                    new Expanded(child:
                        new Image.asset(this.image, fit: BoxFit.contain),
                    ),
                    Text(this.caption, 
                        style: new TextStyle(fontWeight: FontWeight.bold), ), 
                ]),
            ],),
            onPressed: this.cb
        );
        var cont = new Container(child: mb, 
            padding: new EdgeInsets.all(20.0),
        );
        return cont;
    }
}

class _DispatchState extends State<StatefulWidget>
{
    bool loading = true;

    void findCompetion() { 
        Navigator.push(context, 
            MaterialPageRoute(builder: (context) => CompetitionListPage()));
    }

    void callBack() { 
        print ("Ok");
        print (this);
    }

    void newTournament() { 
        print ("Nieuw");
    }

    void editPlayers() { 
        Navigator.push(context, 
            MaterialPageRoute(builder: (context) => PlayersEditPage()));
    } 

    void exitApp() { 
        print ("Bye");
        exit(0);
    }

    @override
    Widget build(BuildContext context) {
        //final TextStyle textStyle = Theme.of(context).textTheme.display1;
    
        // size of the screen, to divide buttons equally over the screen
        var size = MediaQuery.of(context).size;
        final double itemHeight = (size.height - kToolbarHeight - 24) / 2;
        final double itemWidth = size.width / 2;
     
        return new Card(
        color: Color(0XFF404040),
        child: 
            new GridView.count(
                crossAxisCount: 2,
                controller: new ScrollController(keepScrollOffset: false),
                scrollDirection: Axis.vertical,
    
                childAspectRatio: (itemWidth / itemHeight),
                children: [
                        MainButton(caption: "Zoek toernooi", image: 'assets/search.png', color: Color(0XFFa080a0), cb: findCompetion),
                        //new MainButton(caption: "Tournooien", image: 'assets/podium.png', color: Color(0XFF1F8F00), cb: callBack),
                        MainButton(caption: "Nieuw tournooi", image: 'assets/trophy.png',color:  Color(0XFF8080b0),cb: callBack),
                        MainButton(caption: "Spelers", image: 'assets/player.png', color: Color(0XFF6F8F70),cb: editPlayers),
                        //MainButton(caption: "Instellingen", image: 'assets/gear.png', color: Color(0XFF8080a0),cb: callBack),
                        MainButton(caption: "Stoppen", image: 'assets/exit.png',color:  Color(0XFF808080),cb: exitApp),
                ]
            ),
        );
    }
}
