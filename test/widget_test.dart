import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';

import '../lib/login.dart';

void main() {
  testWidgets('my first widget test', (WidgetTester tester) async {
    // You can use keys to locate the widget you need to test
    // Tells the tester to build a UI based on the widget tree passed to it
    await tester.pumpWidget(
      new StatefulBuilder(
        builder: (BuildContext context, StateSetter setState) {
          return new MaterialApp(
            home: new Material(
              child: new LoginPage(title: "Login") 
            ),
          );
        },
      ),
    );

    var iter = tester.allWidgets;
    var count = 0;

    for (var elm in iter) { 
        //print (elm);
        if (elm is LoginPage) count++;
    } 
    expect(count, equals(1));
    

    // Taps on the widget found by key
    //await tester.tap(find.byKey(sliderKey));

    // Verifies that the widget updated the value correctly
    //expect(value, equals(0.5));
  });
}
